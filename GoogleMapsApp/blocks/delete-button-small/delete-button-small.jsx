var React = require('react'),
	BEMHelper = require('react-bem-helper');

var DeleteButtonSmall = React.createClass({
	bemHelper: new BEMHelper('delete-button-small'),

	render: function() {
		var classes = this.bemHelper;
		return (
			<button onClick={this.props.onClick} {...classes()}/>
		);
	}
});

module.exports = DeleteButtonSmall;
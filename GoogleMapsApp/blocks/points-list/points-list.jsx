var React = require('react'),
	BEMHelper = require('react-bem-helper'),
	PointRow = require('./__point-row/points-list__point-row.jsx'),
	ObjectsList = require('./__objects-list/points-list__objects-list.jsx'),
	ObjectRow = require('./__object-row/points-list__object-row.jsx'),
	AppStore = require('../../core/js/flux/app-store.js'),
	Actions = require('../../core/js/flux/actions.js');

var PointsList = React.createClass({
	bemHelper: new BEMHelper('points-list'),

	getInitialState: function() {
		return {
			points: AppStore.getPoints()
		};
	},

	onChange: function() {
		this.setState({points: AppStore.getPoints()});
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this.onChange);
	},

	deletePointRow: function (id) {
		Actions.deletePoint(id);
	},

	renderPointRow: function(point) {
		return (
			<PointRow
				id={point.id}
				key={point.id}>
			</PointRow>
		);
	},

	renderPointRows: function() {
		var renderedPointRows = [];
		for (var id in this.state.points) {
			if (this.state.points.hasOwnProperty(id)) {
				var renderedPointRow = this.renderPointRow(this.state.points[id]);
				renderedPointRows.push(renderedPointRow);
			}
		}
		return renderedPointRows;
	},

	render: function() {
		var classes = this.bemHelper,
			length = Object.keys(this.state.points).length;
		return (
			<ul {...classes()}>
				{ length ? this.renderPointRows() : 'Пока что не добавлено ни одной точки' }
			</ul>
		);
	}
});

module.exports = PointsList;
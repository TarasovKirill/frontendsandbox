var React = require('react'),
	ReactDOM = require('react-dom'),
	BEMHelper = require('react-bem-helper'),
	ObjectsList = require('../__objects-list/points-list__objects-list.jsx'),
	DeleteButton = require('../../delete-button/delete-button.jsx'),
	EditButton = require('../../edit-button/edit-button.jsx'),
	Actions = require('../../../core/js/flux/actions.js'),
	AppStore = require('../../../core/js/flux/app-store.js');

var PointRow = React.createClass({
	bemHelper: new BEMHelper('points-list'),

	getDefaultProps: function() {
		return {
			maxLength: 100
		};
	},

	getInitialState: function () {
		return {
			disabled: true,
			text: AppStore.getPoint(this.props.id).text
		};
	},

	onChange: function() {
		var point = AppStore.getPoint(this.props.id);
		if (point) {
			this.setState({
				text: point.text,
				selected: point.selected
			});
		}
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this.onChange);
	},

	componentDidUpdate: function() {			// working with DOM only after updating component
		var nodeInput = ReactDOM.findDOMNode(this.refs.input);
		if (this.state.disabled) {
			nodeInput.blur();
		}
		else {
			nodeInput.focus();
		}
	},

	deleteHandler: function() {
		Actions.deletePoint(this.props.id);
	},

	enable: function() {
		this.setState({disabled: false});
	},

	disable: function() {
		this.setState({disabled: true});
	},

	editHandler: function() {
		this.enable();
	},

	onBlur: function() {
		this.disable();
	},

	onKeyDown: function(event) {
		if (event.which === 13) {								// Return code
			this.disable();
		}
	},

	onInputChange: function(event) {
		Actions.editPointText(this.props.id, event.target.value);
	},

	selectPoint: function() {
		Actions.selectPoint(this.props.id);
	},

	render: function() {
		var classes = this.bemHelper;
		objectsList = this.state.selected ? <ObjectsList id={this.props.id}/> : '';

		return (
			<li {...classes('point-row')} onClick={this.selectPoint}>
				<input
					ref={'input'}
					disabled={this.state.disabled}
					value={this.state.text}
					onBlur={this.onBlur}
					onKeyDown={this.onKeyDown}
					onChange={this.onInputChange}
					maxLength={this.props.maxLength}/>
				<EditButton onClick={this.editHandler}/>
				<DeleteButton onClick={this.deleteHandler}/>
				{objectsList}
				{this.props.children}
			</li>
		);
	}
});

module.exports = PointRow;
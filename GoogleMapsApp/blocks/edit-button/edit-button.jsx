var React = require('react'),
	BEMHelper = require('react-bem-helper');

var EditButton = React.createClass({
	bemHelper: new BEMHelper('edit-button'),

	render: function() {
		var classes = this.bemHelper;
		return (
			<button onClick={this.props.onClick} {...classes()}/>
		);
	}
});

module.exports = EditButton;
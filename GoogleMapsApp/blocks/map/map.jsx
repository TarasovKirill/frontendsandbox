var StyleArray = require('./style-array.js'),
	React = require('react'),
	BEMHelper = require('react-bem-helper'),
	PropTypes = React.PropTypes,
	GoogleMap = require('google-map-react'),
	Marker = require('./__marker/marker.jsx'),
	AppStore = require('../../core/js/flux/app-store.js'),
	Actions = require('../../core/js/flux/actions.js');

var Map = React.createClass({
	bemHelper: new BEMHelper('map'),

	propTypes: {
		defaultCenter: PropTypes.array,
		zoom: PropTypes.number,
		minMarkerDistance: PropTypes.number
	},

	getDefaultProps: function() {
		return {
			defaultCenter: [56.840560, 60.569094],
			defaultZoom: 13,
			zoom: 13,
			minMarkerDistance: 0.003
		};
	},

	getInitialState: function() {
		return {
			points: AppStore.getPoints(),
			center: this.props.defaultCenter
		};
	},

	onChange: function() {
		var selectedPoint = AppStore.getSelectedPoint();
		this.setState({
			points: AppStore.getPoints(),
			center: selectedPoint ? selectedPoint.coordinates : this.state.center
		});
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this.onChange);
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this.onChange);
	},

	createMapOptions: function() {
		return {
			styles: StyleArray,
			draggableCursor: 'pointer'
		};
	},

	renderMarker: function(point) {
		return (<Marker
					lat={point.coordinates[0]}
					lng={point.coordinates[1]}
					key={point.id}
					id={point.id}/>);
	},

	renderMarkers: function() {
		var renderedMarkers = [];
		for (var id in this.state.points) {
			if (this.state.points.hasOwnProperty(id)) {
				var renderedMarker = this.renderMarker(this.state.points[id]);
				renderedMarkers.push(renderedMarker);
			}
		}
		return renderedMarkers;
	},

	onGoogleMapChange: function(obj) {
		this.setState({
			zoom: obj.zoom,					// needed to updating current map zoom
			center: obj.center   			// and center
		});
	},

	checkDistance: function(point) {		// checking distance between markers before addition of a new point
		for(var id in this.state.points) {
			if (this.state.points.hasOwnProperty(id)) {
				var x1 = this.state.points[id].coordinates[0],
					y1 = this.state.points[id].coordinates[1],
					x2 = point.coordinates[0],
					y2 = point.coordinates[1];

				var distance = Math.sqrt(
					Math.pow((x1 - x2), 2) + 
					Math.pow((y1 - y2), 2)
				);

				// for context distance between points
				var distanceCoofitient = Math.pow(2, (this.state.zoom - this.props.defaultZoom));
				if (distance * distanceCoofitient < this.props.minMarkerDistance) {
					return false;
				}
			}
		}
		return true;
	},

	addPoint: function(point) {
		if (this.checkDistance(point)) {
			Actions.createPoint(point.coordinates);
		}
	},

	handleClick: function(obj) {	// takes event of parent object
		var point = {
			coordinates: [obj.lat, obj.lng]
		};
		this.addPoint(point);
	},

	render: function () {
	var classes = this.bemHelper;
	return (
		<GoogleMap {...classes()}
			onClick={this.handleClick}
			defaultCenter={this.props.defaultCenter}
			defaultZoom={this.props.defaultZoom}
			center={this.state.center}
			options={this.createMapOptions}
			onChange={this.onGoogleMapChange}>
				{this.renderMarkers()}
		</GoogleMap>
		);
	}
});

module.exports = Map;
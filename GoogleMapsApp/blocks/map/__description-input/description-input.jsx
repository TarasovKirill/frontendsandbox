var React = require('react'),
	ReactDOM = require('react-dom'),
	BEMHelper = require('react-bem-helper'),
	Actions = require('../../../core/js/flux/actions.js'),
	AppStore = require('../../../core/js/flux/app-store.js'),
	TextAreaAutosize = require('react-textarea-autosize');

var DescriptionInput = React.createClass({
	bemHelper: new BEMHelper('map'),

	getDefaultProps: function() {
		return {
			maxLength: 100
		};
	},

	getInitialState: function() {
		return {
			disabled: false,
			text: AppStore.getPoint(this.props.id).text
		};
	},

	submit: function(text) {										// submitting text if it isn't empty string
		this.setState({disabled: true});							// disable input
		if (text.trim() === '') {
			Actions.deletePoint(this.props.id);
		}
	},

	onChange: function() {
		var point = AppStore.getPoint(this.props.id);
		if (point) {
			this.setState({text: point.text});
		}
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this.onChange);
		this.setState({focused: true});				// focus on input after mounting
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this.onChange);
	},

	componentDidUpdate: function() {
		var nodeTextarea = ReactDOM.findDOMNode(this.refs.textarea);
		if (this.state.focused) {
			nodeTextarea.focus();
			return;
		}
		nodeTextarea.blur();
	},

	onTextareaChange: function(event) {
		Actions.editPointText(this.props.id, event.target.value); 	// updating our text immediatly
	},

	onKeyDown: function(event) {
		if (event.which === 13) {									// submit when enter pressed
			this.submit(event.target.value);
			this.setState({focused: false});						// and remove focus
		}
	},

	onBlur: function(event) {										// out of focus
		this.setState({focused: false});
		this.submit(event.target.value);
	},

	render: function() {
		var classes = this.bemHelper;

		return (
			<TextAreaAutosize
				disabled={this.state.disabled}
				onKeyDown={this.onKeyDown}
				onChange={this.onTextareaChange}
				onBlur={this.onBlur}
				ref={'textarea'}
				value={this.state.text}
				maxLength={this.props.maxLength}
				{...classes('description-input')}/>
		);
	}
});

module.exports = DescriptionInput;
var React = require('react'),
	BEMHelper = require('react-bem-helper'),
	DescriptionInput = require('../__description-input/description-input.jsx'),
	Actions = require('../../../core/js/flux/actions.js');

var Marker = React.createClass({
	bemHelper: new BEMHelper('map'),

	onClick: function() {
		Actions.selectPoint(this.props.id);
	},

	componentDidMount: function() {
		Actions.selectPoint(this.props.id);
	},

	render: function () {
		var classes = this.bemHelper;
		return (
			<div
				{...classes('marker')}
				onClick={this.onClick}>
					<DescriptionInput id={this.props.id}/>
			</div>
		);
	}
});

module.exports = Marker;
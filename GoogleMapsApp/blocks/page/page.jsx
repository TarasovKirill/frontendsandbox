var React = require('react'),
	BEMHelper = require('react-bem-helper'),
	Map = require('../map/map.jsx'),
	PointsList = require('../points-list/points-list.jsx');

var Page = React.createClass({
	bemHelper: new BEMHelper('page'),

	render: function() {
		var classes = this.bemHelper;
		return (
			<div {...classes()}>
				<Map />
				<PointsList />
			</div>
		);
	}
});

module.exports = Page;

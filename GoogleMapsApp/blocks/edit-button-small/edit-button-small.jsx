var React = require('react'),
	BEMHelper = require('react-bem-helper');

var EditButtonSmall = React.createClass({
	bemHelper: new BEMHelper('edit-button-small'),

	render: function() {
		var classes = this.bemHelper;
		return (
			<button onClick={this.props.onClick} {...classes()}/>
		);
	}
});

module.exports = EditButtonSmall;
var gulp = require('gulp'),
	browserify = require('browserify'),
	source = require('vinyl-source-stream'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	stylus = require('gulp-stylus'),
	watch = require('gulp-watch'),
	jshint = require('gulp-jshint'),
	jshintJSX = require('jshint-jsx')
	stylish = require('jshint-stylish'),
	connect = require('gulp-connect'),
	bemjson = require('gulp-bemjson'),
	bemjsonSource = 'index.bemjson.js';
	jsSource = ['core/js/**/*.js', 'blocks/**/*.js', bemjsonSource],
	jsxSource = ['core/js/**/*.jsx', 'blocks/**/*.jsx'],
	app = 'core/js/app.jsx'
	stylusSource = ['core/styl/*.styl', 'blocks/**/*.styl'];

gulp.task('bemjson', ['validate-js'], function() {
	return gulp.src(bemjsonSource, {read: false})
		.pipe(bemjson())
		.pipe(gulp.dest('static'))
});

gulp.task('connect', function() {
	connect.server({
		root: 'static',
		livereload: true
	});
});

gulp.task('reload', function() {
	gulp.src('static/index.html')
		.pipe(connect.reload());
});

gulp.task('validate-jsx', function() {
	return gulp.src(jsxSource)
		.pipe(jshint({
			linter: jshintJSX.JSXHINT
		}))
		.pipe(jshint.reporter(stylish))
		.pipe(jshint.reporter('fail'));
});

gulp.task('validate-js', function() {
	return gulp.src(jsSource)
		.pipe(jshint())
		.pipe(jshint.reporter(stylish))
		.pipe(jshint.reporter('fail'));
});

gulp.task('browserify', ['validate-js', 'validate-jsx'], function() {
	return browserify(app)
		.bundle()
		.pipe(source('google-maps-app.js'))
		.pipe(gulp.dest('static/js'))
});

gulp.task('compress-js', ['browserify'], function()	{
	return gulp.src('static/js/google-maps-app.js')
		.pipe(uglify())
		.pipe(gulp.dest('static/js'));
});

gulp.task('compress-css', function() {
	return gulp.src(stylusSource)
		.pipe(concat('google-maps-app.styl'))
		.pipe(stylus({ compress: true }))
		.pipe(gulp.dest('static/css'));
});

gulp.task('reload-js', ['compress-js'], function(){
	gulp.run('reload');
});

gulp.task('reload-css', ['compress-css'], function(){
	gulp.run('reload');
});

gulp.task('reload-html', ['bemjson'], function(){
	gulp.run('reload');
});

gulp.task('watch', function() {
	gulp.watch(jsSource.concat(jsxSource), ['reload-js']);
	gulp.watch(stylusSource, ['reload-css']);
	gulp.watch(bemjsonSource, ['reload-html']);
});

gulp.task('default', ['bemjson', 'connect', 'compress-js', 'compress-css', 'watch']);



var AppDispatcher = require('./app-dispatcher.js'),
	EventEmitter = require('events').EventEmitter,
	ActionTypes = require('./action-types.js'),
	assign = require('object-assign');

EventEmitter.prototype._maxListeners = 100;

var CHANGE_EVENT = 'CHANGE';

var _points = {};

function createPoint (coordinates) {
	var id = Date.now();
	_points[id] = {
		id: id,
		coordinates: coordinates,
		text: '',
		selected: false,
		objects: {}
	};
}

function deletePoint(id) {
	delete _points[id];
}

function createObject(pointId, text) {
	var id = Date.now();
	_points[pointId].objects[id] = {
		id: id,
		text: text
	};
}

function deleteObject(pointId, objectId) {
	delete _points[pointId].objects[objectId];
}

function editPointText(id, text) {
	_points[id].text = text;
}

function editObjectText(pointId, objectId, text) {
	_points[pointId].objects[objectId].text = text;
}

function selectPoint(id) {
	for(var pointId in _points) { 					// foreach loop for object
		if (_points.hasOwnProperty(pointId)) {
			_points[pointId].selected = false;
		}
	}
	if (_points.hasOwnProperty(id)) {
		_points[id].selected = true;
	}
}

var AppStore = assign({}, EventEmitter.prototype, {
	getPoints: function() {
		return _points;
	},

	setPoints: function(points) {
		_points = points || {};
	},

	getPoint: function(id) {
		return _points[id];
	},

	getObjects: function(pointId) {
		return _points[pointId] ? _points[pointId].objects : false;
	},

	getObject: function(pointId, objectId) {
		return _points[pointId] ? _points[pointId].objects[objectId] : false;
	},

	getSelectedPoint: function() {
		for (var id in _points) {
			if (_points.hasOwnProperty(id)) {
				if (_points[id].selected) {
					return _points[id];
				}
			}
		}
	},

	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},

	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

	dispatcherIndex: AppDispatcher.register(function(action) {

		switch(action.type) {
			case ActionTypes.CREATE_POINT:
				createPoint(action.coordinates);
				AppStore.emitChange();
				break;

			case ActionTypes.DELETE_POINT:
				deletePoint(action.id);
				AppStore.emitChange();
				break;

			case ActionTypes.CREATE_OBJECT:
				createObject(action.pointId, action.text);
				AppStore.emitChange();
				break;

			case ActionTypes.DELETE_OBJECT:
				deleteObject(action.pointId, action.objectId);
				AppStore.emitChange();
				break;

			case ActionTypes.EDIT_POINT_TEXT:
				editPointText(action.id, action.text);
				AppStore.emitChange();
				break;

			case ActionTypes.EDIT_OBJECT_TEXT:
				editObjectText(action.pointId, action.objectId, action.text);
				AppStore.emitChange();
				break;

			case ActionTypes.SELECT_POINT:
				selectPoint(action.id);
				AppStore.emitChange();
				break;
		}

		return true;		// no errors
	})
});

module.exports = AppStore;